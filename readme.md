
## How to Run this Laravel project

I used Docker to make life easier, so if you're using docker just type the following:

- docker-compose up -d --force-recreate

If by any chance one of the containers does not start just:

- docker-compose restart

That will create a NGINX, MySQL and PHP container in order to run this project.
After that we'll need install some packages such as composer and npm in the root projetcts directory:

- composer install
- npm install
- npm run prod

Composer installs php packages and npm js packages, run prod compiles down the vuejs and other js frameworks.

Once the packages are installed we need to migrate our database:

- php artisan migrate
- php artisan db:seed

Now the databse is created and employees table populated with the seed.
Just head to http://localhost:8090 there you can login / register, since theres no account go ahead and create one.
After that login with your credentials and you be able to see http://localhost:8090/home with the employee data,
will be able to delete and add new employees.
At localhost:8090/employee will list all registered employees on the API returning a JSON.


## Some Notes

At .env is all your configs to run locally or at production.
Also at DB_HOST=  reflects where your mysql is hosted, it is mysql by default,
because at my /etc/hosts has a mysql    127.0.0.1
so mysql is an alias for 127.0.0.1
