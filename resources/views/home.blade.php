@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
                <example-component
                    :item="{{ $employees }}"
                ></example-component>
            </div>
            <br>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Add New Employee</div>
                    <br>

                    <form method="POST" action="/add">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <div class="col-sm-6">
                                <input name="name" type="text" class="form-control" id="name" placeholder="Name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <input name="email" type="email" class="form-control" id="email" placeholder="Email" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6">
                                <input name="department" type="text" class="form-control" id="email" placeholder="Department" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-9">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
