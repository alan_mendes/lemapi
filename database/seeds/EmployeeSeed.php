<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmployeeSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = DB::table('employees')->get();

        if (count($employees) == 0) {
            DB::table('employees')->insert([
                [
                    'name' => 'Arnaldo Pereira',
                    'email' => 'arnaldo@luizalabs.com',
                    'department' => 'Architecture',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
                [
                    'name' => 'Renato Pedigoni',
                    'email' => 'renato@luizalabs.com',
                    'department' => 'E-commerce',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
                [
                    'name' => 'Thiago Catoto',
                    'email' => 'catoto@luizalabs.com',
                    'department' => 'Mobile',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]
            ]);
        }
    }
}
