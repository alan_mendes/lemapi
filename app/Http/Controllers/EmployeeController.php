<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Exception;

class EmployeeController extends Controller
{
    public  $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $employees = Employee::all();

        $listOfEmployees = array();
        foreach ($employees as $employee) {
            /* remove values of return */
            unset($employee->id);
            unset($employee->password);
            unset($employee->created_at);
            unset($employee->updated_at);

            array_push($listOfEmployees, $employee);
        }

        return $listOfEmployees;
    }

    public function add()
    {
        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required',
            'department' => 'required'
        ]);

        try {
            $employee = new Employee;
            $employee->name = $this->request->name;
            $employee->email = $this->request->email;
            $employee->department = $this->request->department;
            $employee->save();

        } catch (Exception $error) {
            return response()->json([
                'error' => $error
            ]);
        }

        return redirect()->route('home');
    }

    public function removeEmployee($id)
    {
        try {
            Employee::find($id)->delete();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
